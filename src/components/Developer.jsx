import "./style.css"

function PersonCard({nome, idade, pais}) {
    return (
        <div className="central">
            <label><strong>Nome:</strong> {nome}</label>
            <label><strong>Idade:</strong> {idade}</label>
            <label><strong>País:</strong> {pais}</label>
        </div>
        )
}

export default PersonCard