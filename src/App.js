import logo from './logo.svg';
import PersonCard from './components/Developer.jsx'
import './App.css';

function App() {

  const Person = {nome: "Gabriel", idade: 28, pais: "Brasil"}
  const Person2 = {nome: "Celso", idade: 53, pais: "Brasil"}
  const Person3 = {nome: "João", idade: 18, pais: "Brasil"}



  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <PersonCard nome={Person.nome} idade={Person.idade} pais={Person.pais}/>
        <PersonCard nome={Person2.nome} idade={Person2.idade} pais={Person2.pais}/>
        <PersonCard nome={Person3.nome} idade={Person3.idade} pais={Person3.pais}/>
      </header>
    </div>
  );
}

export default App;
